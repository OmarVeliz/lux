<?php

class TipoSalasController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        $tipoS= TipoSala::all();

        return View::make('tipoSala.index')
            ->with('tipo', $tipoS);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return View::make('tipoSala.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $rules = array(
            'nombre' => 'required',
            'descripcion' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('tipo/create')
                ->withErrors($validator);
        } else {
            // store
            $tipoS = new TipoSala();
            $tipoS->nombre = Input::get('nombre');
            $tipoS->descripcion = Input::get('descripcion');
            $tipoS->save();

            return Redirect::to('tipo');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        $form = TipoSala::find($id);

        // show the view and pass the nerd to it
        return View::make('tipoSala.show')
            ->with('tipo_detalle', $form);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $nerds = TipoSala::find($id);

        // show the edit form and pass the nerd
        return View::make('tipoSala.edit')
            ->with('tipo_detalle', $nerds);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
        $rules = array(
            'nombre' => 'required',
            'descripcion' => 'required',
        );

        $validatorFormat = Validator::make(Input::all(), $rules);

        // process the login
        if ($validatorFormat->fails()) {
            return Redirect::to('tipo/' . $id . '/edit')
                ->withErrors($validatorFormat);
        } else {
            // store
            $tipoS = TipoSala::find($id);
            $tipoS->nombre = Input::get('nombre');
            $tipoS->descripcion = Input::get('descripcion');
            $tipoS->save();

            return Redirect::to('tipo');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $nerds = TipoSala::find($id);
        $nerds->delete();

        return Redirect::to('tipo');
    }

}
