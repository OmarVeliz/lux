<?php

class CarteleraController extends \BaseController {

    public function index()
    {
        $cartelera = Cartelera::all();

        return View::make('carteleras.index')
            ->with('Cartelera', $cartelera);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('carteleras.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $rules = array(
            'sala_id' => 'required',
            'pelicula_id' => 'required',
            'formatopelicula_id' => 'required',
            'formato_lenguaje' => 'required',
            'fecha' => 'required',
            'hora' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('Carteleras/create')
                ->withErrors($validator);
        } else {
            // store
            $cartelera = new Cartelera();
            $cartelera->sala_id = Input::get('sala_id');
            $cartelera->pelicula_id = Input::get('pelicula_id');
            $cartelera->formatopelicula_id = Input::get('formatopelicula_id');
            $cartelera->formato_lenguaje = Input::get('formato_lenguaje');
            $cartelera->fecha = Input::get('fecha');
            $cartelera->hora = Input::get('hora');
            $cartelera->save();

            return Redirect::to('Carteleras');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $form = Cartelera::find($id);

        // show the view and pass the nerd to it
        return View::make('carteleras.show')
            ->with('carteleras_detalles', $form);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $cartelera = Cartelera::find($id);

        // show the edit form and pass the nerd
        return View::make('carteleras.edit')
            ->with('carteleras_detalles', $cartelera);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $rules = array(
            'sala_id' => 'required',
            'pelicula_id' => 'required',
            'formatopelicula_id' => 'required',
            'formato_lenguaje' => 'required',
            'fecha' => 'required',
            'hora' => 'required',
        );

        $validatorFormat = Validator::make(Input::all(), $rules);

        // process the login
        if ($validatorFormat->fails()) {
            return Redirect::to('Carteleras/' . $id . '/edit')
                ->withErrors($validatorFormat);
        } else {
            // store
            $cartelera = Cartelera::find($id);
            $cartelera->sala_id = Input::get('sala_id');
            $cartelera->pelicula_id = Input::get('pelicula_id');
            $cartelera->formatopelicula_id = Input::get('formatopelicula_id');
            $cartelera->formato_lenguaje = Input::get('formato_lenguaje');
            $cartelera->fecha = Input::get('fecha');
            $cartelera->hora = Input::get('hora');
            $cartelera->save();

            return Redirect::to('Carteleras');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $nerds = Cartelera::find($id);
        $nerds->delete();

        return Redirect::to('Carteleras');
    }
}
