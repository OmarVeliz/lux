<?php

class FormatoController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $formato = FormatoPelicula::all();

        return View::make('formatos.index')
            ->with('FormatoPelicula', $formato);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return View::make('formatos.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $rules = array(
            'nombre' => 'required',
            'descripcion' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('formatos/create')
                ->withErrors($validator);
        } else {
            // store
            $formato = new FormatoPelicula();
            $formato->nombre = Input::get('nombre');
            $formato->descripcion = Input::get('descripcion');
            $formato->save();

            return Redirect::to('formatos');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        $form = FormatoPelicula::find($id);

        // show the view and pass the nerd to it
        return View::make('formatos.show')
            ->with('formato_detalle', $form);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $nerds = FormatoPelicula::find($id);

        // show the edit form and pass the nerd
        return View::make('formatos.edit')
            ->with('formato_detalle', $nerds);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $rules = array(
            'nombre' => 'required',
            'descripcion' => 'required',
        );

        $validatorFormat = Validator::make(Input::all(), $rules);

        // process the login
        if ($validatorFormat->fails()) {
            return Redirect::to('formatos/' . $id . '/edit')
                ->withErrors($validatorFormat);
        } else {
            // store
            $formato = FormatoPelicula::find($id);
            $formato->nombre = Input::get('nombre');
            $formato->descripcion = Input::get('descripcion');
            $formato->save();

            return Redirect::to('formatos');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $nerds = FormatoPelicula::find($id);
        $nerds->delete();

        return Redirect::to('formatos');
    }

}
