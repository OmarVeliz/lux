<?php

class CineController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        $cine = Cine::all();

        return View::make('cines.index')
            ->with('Cine', $cine);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('cines.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $rules = array(
            'nombre' => 'required',
            'direccion' => 'required',
            'telefono' => 'required',
            'latitud' => 'required',
            'longitud' => 'required',
            'hora_apertura' => 'required',
            'hora_cierre' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('cines/create')
                ->withErrors($validator);
        } else {
            // store
            $cine = new Cine();
            $cine->nombre = Input::get('nombre');
            $cine->direccion = Input::get('direccion');
            $cine->telefono = Input::get('telefono');
            $cine->latitud = Input::get('latitud');
            $cine->longitud = Input::get('longitud');
            $cine->hora_apertura = Input::get('hora_apertura');
            $cine->hora_cierre = Input::get('hora_cierre');
            $cine->save();

            return Redirect::to('cines');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        $nerd = Cine::find($id);

        // show the view and pass the nerd to it
        return View::make('cines.show')
            ->with('cine_detalle', $nerd);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $nerd = Cine::find($id);

        // show the edit form and pass the nerd
        return View::make('cines.edit')
            ->with('cine_detalle', $nerd);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
        $rules = array(
            'nombre' => 'required',
            'direccion' => 'required',
            'telefono' => 'required',
            'latitud' => 'required',
            'longitud' => 'required',
            'hora_apertura' => 'required',
            'hora_cierre' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('cines/' . $id . '/edit')
                ->withErrors($validator);
        } else {
            // store
            $cine = Cine::find($id);
            $cine->nombre = Input::get('nombre');
            $cine->direccion = Input::get('direccion');
            $cine->telefono = Input::get('telefono');
            $cine->latitud = Input::get('latitud');
            $cine->longitud = Input::get('longitud');
            $cine->hora_apertura = Input::get('hora_apertura');
            $cine->hora_cierre = Input::get('hora_cierre');
            $cine->save();

            return Redirect::to('cines');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $nerd = Cine::find($id);
        $nerd->delete();

        return Redirect::to('cines');
    }


}
