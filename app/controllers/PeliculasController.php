<?php

class PeliculasController extends \BaseController {


    public function index()
    {
        $pelicula = Peliculas::all();

        return View::make('peliculas.index')
            ->with('Pelicula', $pelicula);
    }


    public function create()
    {
        return View::make('peliculas.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $rules = array(
            'titulo' => 'required',
            'sinopsis' => 'required',
            'trailer_url' => 'required',
            'image' => 'required',
            'rated' => 'required',
            'genero' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('peliculas/create')
                ->withErrors($validator);
        } else {
            // store
            $pelicula = new Peliculas();
            $pelicula->titulo = Input::get('titulo');
            $pelicula->sinopsis = Input::get('sinopsis');
            $pelicula->trailer_url = Input::get('trailer_url');
            $pelicula->image = Input::get('image');
            $pelicula->rated = Input::get('rated');
            $pelicula->genero = Input::get('genero');
            $pelicula->save();

            return Redirect::to('peliculas');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $form = Peliculas::find($id);

        // show the view and pass the nerd to it
        return View::make('peliculas.show')
            ->with('pelicula_detalle', $form);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $nerds = Peliculas::find($id);

        // show the edit form and pass the nerd
        return View::make('peliculas.edit')
            ->with('pelicula_detalle', $nerds);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $rules = array(
            'titulo' => 'required',
            'sinopsis' => 'required',
            'trailer_url' => 'required',
            'image' => 'required',
            'rated' => 'required',
            'genero' => 'required',
        );

        $validatorFormat = Validator::make(Input::all(), $rules);

        // process the login
        if ($validatorFormat->fails()) {
            return Redirect::to('peliculas/' . $id . '/edit')
                ->withErrors($validatorFormat);
        } else {
            // store
            $pelicula = Peliculas::find($id);
            $pelicula->titulo = Input::get('titulo');
            $pelicula->sinopsis = Input::get('sinopsis');
            $pelicula->trailer_url = Input::get('trailer_url');
            $pelicula->image = Input::get('image');
            $pelicula->rated = Input::get('rated');
            $pelicula->genero = Input::get('genero');
            $pelicula->save();

            return Redirect::to('peliculas');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $nerds = Peliculas::find($id);
        $nerds->delete();

        return Redirect::to('peliculas');
    }

}
