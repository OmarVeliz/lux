<!DOCTYPE html>
<html>
<head>
    <title>Salas</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<h1 align="center">Salas</h1>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="list-group">
                <a href="Carteleras" class="list-group-item">Carteleras</a>
                <a href="cines" class="list-group-item">Cines</a>
                <a href="formatos" class="list-group-item">Formatos</a>
                <a href="peliculas" class="list-group-item">Peliculas</a>
                <a href="salas" class="list-group-item active">Salas</a>
                <a href="tipo" class="list-group-item">Tipo sala</a>
            </div>
        </div>
    </div>
</div>

<br>
<h1>_______________________________________________________________________________________________________</h1>
<h1>Salas</h1>
{{ HTML::link(URL::to('salas/create'), 'Agregar') }}
<table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td>Nombre</td>
            <td>Numero</td>
            <td>Tipo</td>
        </tr>
        </thead>
        <tbody>
        @foreach($Sala as $key => $value)
            <tr>
                <td>{{ $value->cine_id }}</td>
                <td>{{ $value->numero }}</td>
                <td>{{ $value->tiposala_id }}</td>
                <td>
                    {{ Form::open(array('url' => 'salas/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                    {{ Form::close() }}

                    <a class="btn btn-small btn-success" href="{{ URL::to('salas/' . $value->id) }}">Show</a>
                     <a class="btn btn-small btn-info" href="{{ URL::to('salas/' . $value->id . '/edit') }}">Edit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>