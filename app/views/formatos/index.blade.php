<!DOCTYPE html>
<html>
<head>
    <title>Mostrar</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<h1 align="center">Formatos</h1>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="list-group">
                <a href="Carteleras" class="list-group-item">Carteleras</a>
                <a href="cines" class="list-group-item">Cines</a>
                <a href="formatos" class="list-group-item active">Formatos</a>
                <a href="peliculas" class="list-group-item">Peliculas</a>
                <a href="salas" class="list-group-item">Salas</a>
                <a href="tipo" class="list-group-item">Tipo sala</a>
            </div>
        </div>
    </div>
</div>

<br>
<h1>_______________________________________________________________________________________________________</h1>
<h1>Todos los formatos</h1>
    {{ HTML::link(URL::to('formatos/create'), 'Agregar') }}

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td>id</td>
            <td>Nombre</td>
            <td>Descripcion</td>
        </tr>
        </thead>
        <tbody>
        @foreach($FormatoPelicula as $key => $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->nombre }}</td>
                <td>{{ $value->descripcion }}</td>
                <td>
                    {{ Form::open(array('url' => 'formatos/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                    {{ Form::close() }}
                    <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                    <a class="btn btn-small btn-success" href="{{ URL::to('formatos/' . $value->id) }}">Show</a>
                    <a class="btn btn-small btn-info" href="{{ URL::to('formatos/' . $value->id . '/edit') }}">Edit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>