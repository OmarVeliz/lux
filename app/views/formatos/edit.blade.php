<!DOCTYPE html>
<html>
<head>
    <title>Editar</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
    <body>
    <div class="container">

        <h1>{{$formato_detalle->nombre }}</h1>
        {{ HTML::ul($errors->all()) }}

        {{ Form::model($formato_detalle, array('route' => array('formatos.update', $formato_detalle->id), 'method' => 'PUT')) }}

        <div class="form-group">
            {{ Form::label('nombre', 'Nombre') }}
            {{ Form::text('nombre', null, array('class' => 'form-control')) }}
            {{ Form::label('descripcion', 'Descripcion') }}
            {{ Form::text('descripcion', null, array('class' => 'form-control')) }}
        </div>

        {{ Form::submit('Finalizar', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}

    </div>
    </body>
</html>