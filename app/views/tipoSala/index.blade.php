<!DOCTYPE html>
<html>
<head>
    <title>Tipo</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<h1 align="center">Tipo sala</h1>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="list-group">
                <a href="Carteleras" class="list-group-item">Carteleras</a>
                <a href="cines" class="list-group-item">Cines</a>
                <a href="formatos" class="list-group-item">Formatos</a>
                <a href="peliculas" class="list-group-item">Peliculas</a>
                <a href="salas" class="list-group-item">Salas</a>
                <a href="tipo" class="list-group-item active">Tipo sala</a>
            </div>
        </div>
    </div>
</div>

<br>
<h1>_______________________________________________________________________________________________________</h1>
<h1>Tipos</h1>
{{ HTML::link(URL::to('tipo/create'), 'Agregar nuevo tipo') }}
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td>id</td>
            <td>Nombre</td>
            <td>Descripcion</td>
        </tr>
        </thead>
        <tbody>
        @foreach($tipo as $key => $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->nombre }}</td>
                <td>{{ $value->descripcion }}</td>

                <td>
                    {{ Form::open(array('url' => 'tipo/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                    {{ Form::close() }}
                    <a class="btn btn-small btn-success" href="{{ URL::to('tipo/' . $value->id) }}">Show</a>
                    <a class="btn btn-small btn-info" href="{{ URL::to('tipo/' . $value->id . '/edit') }}">Edit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>