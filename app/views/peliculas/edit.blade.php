<!DOCTYPE html>
<html>
<head>
    <title>Editar formato</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
    <body>
    <div class="container">

        <h1>{{$pelicula_detalle->nombre }}</h1>
        {{ HTML::ul($errors->all()) }}
        {{ Form::model($pelicula_detalle, array('route' => array('peliculas.update', $pelicula_detalle->id), 'method' => 'PUT')) }}

        <div class="form-group">
            {{ Form::label('titulo', 'Titulo') }}
            {{ Form::text('titulo', null, array('class' => 'form-control')) }}
            {{ Form::label('sinopsis', 'Sinopsis') }}
            {{ Form::text('sinopsis',null, array('class' => 'form-control')) }}
            {{ Form::label('trailer_url', 'Trailer url') }}
            {{ Form::text('trailer_url', null, array('class' => 'form-control')) }}
            {{ Form::label('image', 'Image') }}
            {{ Form::text('image', null, array('class' => 'form-control')) }}
            {{ Form::label('rated', 'Rated url') }}
            {{ Form::text('rated',null, array('class' => 'form-control')) }}
            {{ Form::label('genero', 'Genero') }}
            {{ Form::text('genero', null, array('class' => 'form-control')) }}
        </div>

        {{ Form::submit('Finalizar', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}
    </div>
    </body>
</html>