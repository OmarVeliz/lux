<!DOCTYPE html>
<html>
<head>
    <title>Agregar</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
    <body>
    <div class="container">

        <h1>Agregar pelicula</h1>
        {{ HTML::ul($errors->all()) }}
        {{ Form::open(array('url' => 'peliculas')) }}

        <div class="form-group">
            {{ Form::label('titulo', 'Titulo') }}
            {{ Form::text('titulo', Input::old('titulo'), array('class' => 'form-control')) }}
            {{ Form::label('sinopsis', 'Sinopsis') }}
            {{ Form::text('sinopsis', Input::old('sinopsis'), array('class' => 'form-control')) }}
            {{ Form::label('trailer_url', 'Trailer url') }}
            {{ Form::text('trailer_url', Input::old('trailer_url'), array('class' => 'form-control')) }}
            {{ Form::label('image', 'Image') }}
            {{ Form::text('image', Input::old('image'), array('class' => 'form-control')) }}
            {{ Form::label('rated', 'Rated url') }}
            {{ Form::text('rated', Input::old('rated'), array('class' => 'form-control')) }}
            {{ Form::label('genero', 'Genero') }}
            {{ Form::text('genero', Input::old('genero'), array('class' => 'form-control')) }}
        </div>

        {{ Form::submit('Agregar', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}
    </div>
    </body>
</html>