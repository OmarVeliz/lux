<!DOCTYPE html>
<html>
<head>
    <title>Mostrar</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<h1 align="center">Peliculas</h1>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="list-group">
                <a href="Carteleras" class="list-group-item">Carteleras</a>
                <a href="cines" class="list-group-item">Cines</a>
                <a href="formatos" class="list-group-item">Formatos</a>
                <a href="peliculas" class="list-group-item active">Peliculas</a>
                <a href="salas" class="list-group-item">Salas</a>
                <a href="tipo" class="list-group-item">Tipo sala</a>
            </div>
        </div>
    </div>
</div>

<br>
<h1>_______________________________________________________________________________________________________</h1>
<h1>Todas las peliculas</h1>
{{ HTML::link(URL::to('peliculas/create'), 'Agregar pelicula') }}
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td>id</td>
            <td>Titulo</td>
            <td>Sinopsis</td>
            <td>Trailer</td>
            <td>Image</td>
            <td>Rated</td>
            <td>Genero</td>
        </tr>
        </thead>
        <tbody>
        @foreach($Pelicula as $key => $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->titulo }}</td>
                <td>{{ $value->sinopsis }}</td>
                <td>{{$value->trailer_url}}</td>
                <td>{{ $value -> image}}</td>
                <td>{{ $value->rated }}</td>
                <td>{{ $value->genero }}</td>
                <td>

                    {{ Form::open(array('url' => 'peliculas/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                    {{ Form::close() }}

                    <a class="btn btn-small btn-success" href="{{ URL::to('peliculas/' . $value->id) }}">Show</a>
                    <a class="btn btn-small btn-info" href="{{ URL::to('peliculas/' . $value->id . '/edit') }}">Edit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>