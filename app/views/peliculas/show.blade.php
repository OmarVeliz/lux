<!DOCTYPE html>
<html>
<head>
    <title>Mostrar todas las peliculas</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
    <body>
    <div class="container">

        <h1>{{ $pelicula_detalle->titulo }}</h1>

        <div class="jumbotron text-center">
            <h2>{{$pelicula_detalle->nombre }}</h2>
            <p>
                <strong>Titulo:</strong> {{ $pelicula_detalle->titulo }}<br>
            </p>
        </div>
    </div>
    </body>
</html>