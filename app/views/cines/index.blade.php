<!DOCTYPE html>
<html>
<head>
    <title>Ver</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<h1 align="center">Cines</h1>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="list-group">
                <a href="Carteleras" class="list-group-item">Carteleras</a>
                <a href="cines" class="list-group-item active">Cines</a>
                <a href="formatos" class="list-group-item">Formatos</a>
                <a href="peliculas" class="list-group-item">Peliculas</a>
                <a href="salas" class="list-group-item">Salas</a>
                <a href="tipo" class="list-group-item">Tipo sala</a>
            </div>
        </div>
    </div>
</div>

<br>
<h1>_______________________________________________________________________________________________________</h1>
{{ HTML::link(URL::to('cines/create'), 'Agregar cine') }}
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <td>id</td>
        <td>Nombre</td>
        <td>Direccion</td>
        <td>Telefono</td>
        <td>Latitud</td>
        <td>Longitud</td>
        <td>Abierto</td>
        <td>Cerrado</td>
    </tr>
    </thead>
    <tbody>
    @foreach($Cine as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->nombre }}</td>
            <td>{{ $value->direccion }}</td>
            <td>{{ $value->telefono }}</td>
            <td>{{ $value->latitud }}</td>
            <td>{{ $value->longitud }}</td>
            <td>{{ $value->hora_apertura }}</td>
            <td>{{ $value->hora_cierre }}</td>
            <!-- we will also add show, edit, and delete buttons -->
            <td>
                {{ Form::open(array('url' => 'cines/' . $value->id, 'class' => 'pull-right')) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}
                <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                <a class="btn btn-small btn-success" href="{{ URL::to('cines/' . $value->id) }}">Show</a>

                <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                <a class="btn btn-small btn-info" href="{{ URL::to('cines/' . $value->id . '/edit') }}">Edit</a>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>