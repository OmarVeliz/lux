<!DOCTYPE html>
<html>
<head>
    <title>Agregar</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<h1 align="center">Carteleras</h1>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="list-group">
                <a href="Carteleras" class="list-group-item active">Carteleras</a>
                <a href="cines" class="list-group-item">Cines</a>
                <a href="formatos" class="list-group-item">Formatos</a>
                <a href="peliculas" class="list-group-item">Peliculas</a>
                <a href="salas" class="list-group-item">Salas</a>
                <a href="tipo" class="list-group-item">Tipo sala</a>
            </div>
        </div>
    </div>
</div>

<br>
<h1>_______________________________________________________________________________________________________</h1>
{{ HTML::link(URL::to('Carteleras/create'), 'Cartelera') }}
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <td>Sala</td>
        <td>Pelicula</td>
        <td>Formato</td>
        <td>Lenguaje</td>
        <td>Fecha</td>
        <td>Hora</td>
    </tr>
    </thead>
    <tbody>
    @foreach($Cartelera as $key => $value)
        <tr>
            <td>{{ $value->sala_id }}</td>
            <td>{{ $value->pelicula_id }}</td>
            <td>{{ $value->formatopelicula_id }}</td>
            <td>{{ $value->formato_lenguaje }}</td>
            <td>{{ $value->fecha }}</td>
            <td>{{ $value->hora }}</td>
            <td>
                {{ Form::open(array('url' => 'Carteleras/' . $value->id, 'class' => 'pull-right')) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}
                <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                <a class="btn btn-small btn-success" href="{{ URL::to('Carteleras/' . $value->id) }}">Show</a>

                <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                <a class="btn btn-small btn-info" href="{{ URL::to('Carteleras/' . $value->id . '/edit') }}">Edit</a>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>