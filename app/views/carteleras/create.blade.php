<!DOCTYPE html>
<html>
<head>
    <title>Agregar</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
    <body>
    <div class="container">


        <h1>Carteleras</h1>

        {{ HTML::ul($errors->all()) }}
        {{ Form::open(array('url' => 'Carteleras')) }}

        <div class="form-group">
            {{ Form::label('sala_id', 'Sala id') }}
            {{ Form::text('sala_id', Input::old('sala_id'), array('class' => 'form-control')) }}
            {{ Form::label('pelicula_id', 'Pelicula id') }}
            {{ Form::text('pelicula_id', Input::old('pelicula_id'), array('class' => 'form-control')) }}
            {{ Form::label('formatopelicula_id', 'Formatopelicula id') }}
            {{ Form::text('formatopelicula_id', Input::old('formatopelicula_id'), array('class' => 'form-control')) }}
            {{ Form::label('formato_lenguaje', 'Formato_lenguaje') }}
            {{ Form::text('formato_lenguaje', Input::old('formato_lenguaje'), array('class' => 'form-control')) }}
            {{ Form::label('fecha', 'Fecha') }}
            {{ Form::text('fecha', Input::old('fecha'), array('class' => 'form-control')) }}
            {{ Form::label('hora', 'Hora') }}
            {{ Form::text('hora', Input::old('hora'), array('class' => 'form-control')) }}
        </div>

        {{ Form::submit('Agregar', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}
    </div>
    </body>
</html>