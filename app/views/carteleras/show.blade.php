<!DOCTYPE html>
<html>
<head>
    <title>Mostrar</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
    <body>
    <div class="container">

        <h1>{{ $carteleras_detalles->formato_lenguaje }}</h1>

        <div class="jumbotron text-left">
            <h2>{{$carteleras_detalles->formato_lenguaje }}</h2>
            <p>
                <strong>Fecha:</strong> {{ $carteleras_detalles->fecha }}<br>
            </p>
        </div>

    </div>
    </body>
</html>