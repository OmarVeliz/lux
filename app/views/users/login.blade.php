<!DOCTYPE html>
<html>
<head>
    <title>Bienvenido</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
{{ Form::open(array('url'=>'user/signin', 'class'=>'form-signin')) }}

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center">
               Bienvenido
                <a class="navbar-brand" href="{{ URL::to('user/register') }}">Registrarme</a>
            </h3>
        </div>
    </div>
</div>
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
{{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'Email Address')) }}
{{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}
<br>
{{ Form::submit('Login', array('class'=>'btn btn-large btn-primary btn-block'))}}
{{ Form::close() }}
</body>
</html>