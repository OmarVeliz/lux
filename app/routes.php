<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

//Rutas a las views
Route::get('/', 'CineController@index');
Route::get('/', 'carteleraController@index');
Route::get('/', 'FormatoController@index');
Route::get('/', 'PeliclasController@index');
Route::get('/', 'SalasController@index');
Route::get('/', 'TipoSalasController@index');

//Rutas a los controladores
Route::resource('cines','CineController');
Route::resource('Carteleras','CarteleraController');
Route::resource('formatos','FormatoController');
Route::resource('peliculas','PeliculasController');
Route::resource('salas','SalasController');
Route::resource('tipo','TipoSalasController');
Route::controller('user', 'UsersController');